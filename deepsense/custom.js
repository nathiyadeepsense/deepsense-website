$(document).ready(function(){
      $(".main").onepage_scroll({
        sectionContainer: "section",
        animationTime: 700,
        pagination: false,
        updateURL: false,
        responsiveFallback: false,
        loop: false
      });
    });
    
    $("#arrow").click(function(){
      $(".main").moveTo(2);
      });


    $(".p3").click(function(){
    $(".main").moveTo(3);
    });
   
   $(".p4").click(function(){
    $(".main").moveTo(4);
    });

    $(".p5").click(function(){
    $(".main").moveTo(5);
    });

    $(".p6").click(function(){
    $(".main").moveTo(6);
    });

    $(".p7").click(function(){
    $(".main").moveTo(7);
    });

    $(".p8").click(function(){
    $(".main").moveTo(8);
    });

  
    

var TxtType = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
      this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

if (!this.isDeleting && this.txt === fullTxt) {
  delta = this.period;
  this.isDeleting = true;
} else if (this.isDeleting && this.txt === '') {
  this.isDeleting = false;
  this.loopNum++;
  delta = 500;
}

  setTimeout(function() {
  that.tick();
}, delta);
};

window.onload = function() {
var elements = document.getElementsByClassName('typewrite');
for (var i=0; i<elements.length; i++) {
var toRotate = elements[i].getAttribute('data-type');
var period = elements[i].getAttribute('data-period');
if (toRotate) {
new TxtType(elements[i], JSON.parse(toRotate), period);
}
}
// INJECT CSS
var css = document.createElement("style");
css.type = "text/css";
css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
document.body.appendChild(css);
};

if ($(window).width() > 768) {
      $(".one").hover(function(){ 
        $('#brief').html('<b>Search Engine Optimization</b></br> We strive to ensure that our services provide top rankings on business keywords to drive organic traffic and build a strong Brand Identity.');
      });
}else{
  $(".one").click(function(){
      $('#brief').html('<b>Search Engine Optimization</b></br> We strive to ensure that our services provide top rankings on business keywords to drive organic traffic and build a strong Brand Identity.');
  });
}

if ($(window).width() > 768) {
      $(".two").hover(function(){ 
        $('#brief').html('<b>Social Media Marketing</b></br> From Social Media post to Ad Campaigns, we do it all! We at DeepSense, guarantee active engagement on all major platforms like Facebook, Instagram, Twitter, YouTube, Pinterest and Blogs, creating a complete network of engaged audience. And the ship doesn’t stop there, we also ensure you achieve your sales objective.');
      });
}else{
  $(".two").click(function(){
      $('#brief').html('<b>Social Media Marketing</b></br> From Social Media post to Ad Campaigns, we do it all! We at DeepSense, guarantee active engagement on all major platforms like Facebook, Instagram, Twitter, YouTube, Pinterest and Blogs, creating a complete network of engaged audience. And the ship doesn’t stop there, we also ensure you achieve your sales objective.');
  });
}

if ($(window).width() > 768) {
      $(".three").hover(function(){ 
        $('#brief').html('<b>Mobile Apps</b></br> We don’t do it from a developer angle. We work on customer experience angle. We develop mobile applications on various platforms like Android, IOS etc. and provide a rich and interactive experience.');
      });
}else{
  $(".three").click(function(){
      $('#brief').html('<b>Mobile Apps</b></br> We don’t do it from a developer angle. We work on customer experience angle. We develop mobile applications on various platforms like Android, IOS etc. and provide a rich and interactive experience.');
  });
}

if ($(window).width() > 768) {
      $(".four").hover(function(){ 
        $('#brief').html('<b>Loyalty</b></br> We don’t just build links; we build relationships. With a complete package of loyalty programs and schemes, we ensure happy dealers, employees, clients and customers.');
      });
}else{
  $(".four").click(function(){
      $('#brief').html('<b>Loyalty</b></br> We don’t just build links; we build relationships. With a complete package of loyalty programs and schemes, we ensure happy dealers, employees, clients and customers.');
  });
}

if ($(window).width() > 768) {
      $(".five").hover(function(){ 
        $('#brief').html('<b>Software Development</b></br> Our skills provide innovative and high-quality software ranging from desktop applications to intranet and ensure robust outputs.');
      });
}else{
  $(".five").click(function(){
      $('#brief').html('<b>Software Development</b></br> Our skills provide innovative and high-quality software ranging from desktop applications to intranet and ensure robust outputs.');
  });
}

if ($(window).width() > 768) {
      $(".six").hover(function(){ 
        $('#brief').html('<b>Creative Designing</b></br> Driven by a flair for creativity and exploring exciting opportunities, we provide a wide variety of design solutions including website design and multi-media design.With a combination of thoughts and advanced web technologies, we create the best UX.');
      });
}else{
  $(".six").click(function(){
      $('#brief').html('<b>Creative Designing</b></br> Driven by a flair for creativity and exploring exciting opportunities, we provide a wide variety of design solutions including website design and multi-media design.With a combination of thoughts and advanced web technologies, we create the best UX.');
  });
}
